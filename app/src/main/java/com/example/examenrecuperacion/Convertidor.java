package com.example.examenrecuperacion;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class Convertidor extends AppCompatActivity {
    private EditText txtCantidad;
    private RadioButton rdbCel, rdbFar;
    private TextView lblResultado,lblNombre;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_convertidor);
        String name = getIntent().getStringExtra("nombre");
        txtCantidad = findViewById(R.id.txtCantidad);
        rdbCel = findViewById(R.id.rdbCel);
        rdbFar = findViewById(R.id.rdbFar);
        lblResultado = findViewById(R.id.lblResultado);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        lblNombre = findViewById(R.id.lblNombre);
        lblResultado = findViewById(R.id.lblResultado);

        String nombre = getIntent().getStringExtra("nombre");

        lblNombre.setText("Bienvenido: "+nombre);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                lblResultado.setText("");
            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cantidadStr = txtCantidad.getText().toString();
                if (!cantidadStr.isEmpty()) {
                    double cantidad = Double.parseDouble(cantidadStr);
                    double resultado;
                    if (rdbCel.isChecked()) {
                        resultado = (cantidad * 9 / 5) + 32;
                        String mostrarRes = String.format("%2.2f",Math.ceil(resultado*100)/100);
                        lblResultado.setText("Resultado: " + mostrarRes + " °F");
                    } else if (rdbFar.isChecked()) {
                        resultado = (cantidad - 32) * 5 / 9;
                        String mostrarRes = String.format("%2.2f",Math.ceil(resultado*100)/100);
                        lblResultado.setText("Resultado: " + mostrarRes + " °C");
                    }
                }else{
                    Toast.makeText(Convertidor.this,"El campo debe de ser completado", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

}

