package com.example.examenrecuperacion;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class IMC extends AppCompatActivity {

    private TextView lblResultado, lblNombre;
    private EditText txtMetros,txtCentimetros,txtPeso;
    private Button btnCalcular, btnLimpiar , btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imc);

        lblResultado = (TextView) findViewById(R.id.lblRes);
        txtMetros = (EditText) findViewById(R.id.txtMetros);
        txtCentimetros = (EditText) findViewById(R.id.txtCm);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        lblNombre= (TextView) findViewById(R.id.lblNombre);
        String nombre = getIntent().getStringExtra("nombre");
        lblNombre.setText("Bienvenido: "+nombre);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPeso.setText("");
                txtCentimetros.setText("");
                txtMetros.setText("");
                lblResultado.setText("");

            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtpeso = txtPeso.getText().toString();
                String txtcentimetros = txtCentimetros.getText().toString();
                String txtmetros = txtMetros.getText().toString();
                if(txtcentimetros.isEmpty() ||txtmetros.isEmpty()  || txtpeso.isEmpty()){
                    Toast.makeText(IMC.this,"Ambos campos deben de estar completos", Toast.LENGTH_LONG).show();
                }else{
                    float altura = Float.parseFloat(txtmetros) + (Float.parseFloat(txtcentimetros)/100);
                    float peso = Float.parseFloat(txtpeso);

                    float imc =  peso/ (altura * altura);
                    String resultado = String.format("%2.2f",Math.ceil(imc*100)/100);

                    lblResultado.setText(resultado);
                }


            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}
